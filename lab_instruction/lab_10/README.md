# Lab 10 : Advanced Implementation _Cookie_ and _Session_

CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *

## Objectives

After finishing this tutorial, students are expected to understand:
- How to use OMDb API (The Open Movie Database)
- How to use plug-in DataTables
- How to use Session and Model to store data

## End Result
- Use Login feature
- Show movie list from OMDb API
- Create a movie search function
- Add a feature "Watch Later" using session and model


## Self-Reflection
To be able to work on this tutorial, you need to remember and understand :

1. How to access and use data with the correct data types ( dic, list, json, etc)

1. How to use session (adding and deleting items from session)

1.how to use Django Model (adding and deleting items)


## Additional Information
#### OMDB API
Lab 10 tutorial uses [OMDb API](http://www.omdbapi.com/) to obtain movie list. 
to be able to use this API, you need to obtain [API KEY](http://www.omdbapi.com/apikey.aspx)
Learn how to obtain the data and implement it

#### DataTables
Datatables are one of the _powerful_ Jquery _plugin_. In this tutorial, you will learn
**one of the ways** using DataTables, which is Ajax from
[custom data property](https://datatables.net/examples/ajax/custom_data_property.html). 


## Warning
On this lab, you are expected to be creative. This Lab only gives you _template_
and basic instructions. Naming on HTML or Python files and also its location on the _project_ structure are
up to you. We trust that you are already good enough on creating a good website with your own creativity

## Creating Application page: Login & Dashboard
1. Do this configuration step to create Lab 10 (same as past labs)

1. Create URL configuration in `lab_10/urls.py`:
    ```python
    from django.conf.urls import url
    from .views import *
    
    from .custom_auth import auth_login, auth_logout
    
    urlpatterns = [
        # custom auth
        url(r'^custom_auth/login/$', auth_login, name='auth_login'),
        url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
    
        # index dan dashboard
        url(r'^$', index, name='index'),
        url(r'^dashboard/$', dashboard, name='dashboard'),
    
        #movie
        url(r'^movie/list/$', movie_list, name='movie_list'),
        url(r'^movie/detail/(?P<id>.*)/$', movie_detail, name='movie_detail'),
    
        # Session dan Model (Watch Later)
        url(r'^movie/watch_later/add/(?P<id>.*)/$', add_watch_later, name='add_watch_later'),
        url(r'^movie/watch_later/$', list_watch_later, name='list_watch_later'),
    
        #API
        url(r'^api/movie/(?P<judul>.*)/(?P<tahun>.*)/$', api_search_movie, name='api_search_movie'),
    ]
    ```
1. Use the file `csui_helper.py` on Lab 9 for login function

1. Use the file `custom_auth.py` on lab 9 for authentication function
> Note : Please do a code change as you need

1. Insert this code into `views.py``

    ```python
    # -*- coding: utf-8 -*-
    from __future__ import unicode_literals
    
    import json
    
    from django.contrib import messages
    from django.http import HttpResponseRedirect, HttpResponse
    from django.shortcuts import render
    from django.urls import reverse
    
    from .omdb_api import get_detail_movie, search_movie
    from .utils import *
    response = {}
    
    # Create your views here.
    
    ### USER
    def index(request):
        # print ("#==> masuk index")
        if 'user_login' in request.session:
            return HttpResponseRedirect(reverse('lab-10:dashboard'))
        else:
            response['author'] = get_data_user(request, 'user_login')
            html = 'lab_10/login.html'
            return render(request, html, response)
    
    def dashboard(request):
        print ("#==> dashboard")
    
        if not 'user_login' in request.session.keys():
            return HttpResponseRedirect(reverse('lab-10:index'))
        else:
            set_data_for_session(request)
            kode_identitas = get_data_user(request, 'kode_identitas')
            try:
                pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
            except Exception as e:
                pengguna = create_new_user(request)
    
            movies_id = get_my_movies_from_session(request)
            save_movies_to_database(pengguna, movies_id)
    
            html = 'lab_10/dashboard.html'
            return render(request, html, response)
    
    
    ### MOVIE : LIST and DETAIL
    def movie_list(request):
        judul, tahun = get_parameter_request(request)
        urlDataTables = "/lab-10/api/movie/" + judul + "/" + tahun
        jsonUrlDT = json.dumps(urlDataTables)
        response['jsonUrlDT'] = jsonUrlDT
        response['judul'] = judul
        response['tahun'] = tahun
    
        get_data_session(request)
    
        html = 'lab_10/movie/list.html'
        return render(request, html, response)
    
    def movie_detail(request, id):
        print ("MOVIE DETAIL = ", id)
        response['id'] = id
        if get_data_user(request, 'user_login'):
            is_added = check_movie_in_database(request, id)
        else:
            is_added = check_movie_in_session(request, id)
    
        response['added'] = is_added
        response['movie'] = get_detail_movie(id)
        html = 'lab_10/movie/detail.html'
        return render(request, html, response)
    
    ### WATCH LATER : ADD and LIST
    def add_watch_later(request, id):
        print ("ADD WL => ", id)
        msg = "Berhasil tambah movie ke Watch Later"
        if get_data_user(request, 'user_login'):
            print ("TO DB")
            is_in_db = check_movie_in_database(request, id)
            if not is_in_db:
                add_item_to_database(request, id)
            else:
                msg = "Movie already exist on DATABASE! Hacking detected!"
        else:
            print ("TO SESSION")
            is_in_ssn = check_movie_in_session(request, id)
            if not is_in_ssn:
                add_item_to_session(request, id)
            else:
                msg = "Movie already exist on SESSION! Hacking detected!"
    
        messages.success(request, msg)
        return HttpResponseRedirect(reverse('lab-10:movie_detail', args=(id,)))
    
    def list_watch_later(request):
        #  Implement this function by yourself
        get_data_session(request)
        moviesku = []
        if get_data_user(request, 'user_login'):
            moviesku = get_my_movies_from_database(request)
        else:
            moviesku = get_my_movies_from_session(request)
    
        watch_later_movies = get_list_movie_from_api(moviesku)
    
        response['watch_later_movies'] = watch_later_movies
        html = 'lab_10/movie/watch_later.html'
        return render(request, html, response)
    
    ### SESSION : GET and SET
    def get_data_session(request):
        if get_data_user(request, 'user_login'):
            response['author'] = get_data_user(request, 'user_login')
    
    def set_data_for_session(request):
        response['author'] = get_data_user(request, 'user_login')
        response['kode_identitas'] = request.session['kode_identitas']
        response['role'] = request.session['role']
    
    
    ### API : SEARCH movie
    def api_search_movie(request, judul, tahun):
        print ("API SEARCH MOVIE")
        if judul == "-" and tahun == "-":
            items = []
        else:
            search_results = search_movie(judul, tahun)
            items = search_results
    
        dataJson = json.dumps({"dataku":items})
        mimetype = 'application/json'
        return HttpResponse(dataJson, mimetype)

    ```

1. Insert this code below into `models.py``
    ```python
    # -*- coding: utf-8 -*-
    from __future__ import unicode_literals
    
    from django.db import models
    
    # Create your models here.
    class Pengguna(models.Model):
        kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, )
        nama = models.CharField('Nama', max_length=200)
        created_at = models.DateTimeField(auto_now_add=True)
        updated_at = models.DateTimeField(auto_now=True)
    
    class MovieKu(models.Model):
        pengguna = models.ForeignKey(Pengguna)
        kode_movie = models.CharField("Kode Movie", max_length=50)
        created_at = models.DateTimeField(auto_now_add=True)
        updated_at = models.DateTimeField(auto_now=True)
    ```
1. Create a file `omdb_api.py` inside `lab_10`
    ```python
    import requests
    API_KEY = "" #TODO Implement, fill your OMDB API Key Here
    
    def search_movie(judul, tahun):
        print ("METHOD SEARCH MOVIE")
        get_tahun = ""
        if not tahun == "-":
            get_tahun = "&y="+tahun
        url = "http://www.omdbapi.com/?s=" + judul + get_tahun + "&apikey=" + API_KEY ;
        req = requests.get(url)
        resp = req.json()
    
        data_exist = False
        stResponse = resp['Response']
        print ("RESPONSE => ", stResponse)
        if stResponse == "True":
            count_results = resp['totalResults']
    
            #cukup ambil 30 data saja
            cp = (int(count_results) / 10)
            if cp > 3: pages = 3
            elif cp > 0 and cp <= 3: pages = cp
            else: pages = 1
            data_exist = True
    
        past_url = url
        all_data = []
        if data_exist:
            for page in range(pages):
                page += 1
                get_page = "&page="+str(page)
                new_url = past_url + get_page;
                new_req = requests.get(new_url).json()
                get_datas = new_req['Search']
                for data in get_datas:
                    all_data.append(data)
    
        return all_data
    
    
    def get_detail_movie(id):
        url = "http://www.omdbapi.com/?i="+id+"&apikey="+API_KEY;
        req = requests.get(url)
        rj = req.json() # dict
        my_list = create_json_from_dict(rj)
    
        return my_list
    
    def create_json_from_dict(your_dict):
        your_data = {}
        for key in your_dict:
            cvalue = (your_dict.get(key))
            nk = str(key).lower()
            if type(cvalue) == list:
                nv = cvalue
            else:
                nv = cvalue.encode('ascii','ignore')
            your_data[nk] = nv
        return your_data
    ```
1. Create `utils.py` inside `lab_10`
    ```python
    
    from .models import Pengguna, MovieKu
    from .omdb_api import get_detail_movie
    
    def check_movie_in_database(request, kode_movie):
        is_exist = False
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
        count_movie = MovieKu.objects.filter(pengguna=pengguna, kode_movie=kode_movie).count()
        if count_movie > 0 :
            is_exist = True
    
        return is_exist
    
    def check_movie_in_session(request, kode_movie):
        is_exist = False
        ssn_key = request.session.keys()
        if 'movies' in ssn_key:
            movies = request.session['movies']
            if kode_movie in movies:
                is_exist = True
    
        return is_exist
    
    def add_item_to_database(request, id):
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
        movieku = MovieKu()
        movieku.kode_movie = id
        movieku.pengguna = pengguna
        movieku.save()
    
    def add_item_to_session(request, id):
        ssn_key = request.session.keys()
        if not 'movies' in ssn_key:
            request.session['movies'] = [id]
        else:
            movies = request.session['movies']
            # check apakah di session sudah ada key yang sama
            if id not in movies:
                movies.append(id)
                request.session['movies'] = movies
    
    
    def get_data_user(request, tipe):
        data = None
        if tipe == "user_login" and 'user_login' in request.session:
            data = request.session['user_login']
        elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
            data = request.session['kode_identitas']
    
        return data
    
    def create_new_user(request):
        nama = get_data_user(request, 'user_login')
        kode_identitas = get_data_user(request, 'kode_identitas')
    
        pengguna = Pengguna()
        pengguna.kode_identitas = kode_identitas
        pengguna.nama = nama
        pengguna.save()
    
        return pengguna
    
    def get_parameter_request(request):
        if request.GET.get("judul"):
            judul = request.GET.get("judul")
        else:
            judul = "-"
    
        if request.GET.get("tahun"):
            tahun = request.GET.get("tahun")
        else:
            tahun = "-"
    
        return judul, tahun
    
    # after login, save movies from session
    def save_movies_to_database(pengguna, list_movie_id):
        #looping get id, cek apakah exist berdasarkan user, jika tidak ada, maka tambah
    
        for movie_id in list_movie_id:
            if not (MovieKu.objects.filter(pengguna = pengguna, kode_movie = movie_id).count()) > 0:
                new_movie = MovieKu()
                new_movie.pengguna = pengguna
                new_movie.kode_movie = movie_id
                new_movie.save()
    
    #return movies user from db
    def get_my_movies_from_database(request):
        resp = []
        kode_identitas = get_data_user(request, 'kode_identitas')
        pengguna = Pengguna.objects.get(kode_identitas=kode_identitas)
        items = MovieKu.objects.filter(pengguna=pengguna)
        for item in items:
            resp.append(item.kode_movie)
        return resp
    
    #get my movies from session
    def get_my_movies_from_session(request):
        resp = []
        ssn_key = request.session.keys()
        if 'movies' in ssn_key:
            resp = request.session['movies']
        return resp
    
    #get detail list movie from api
    def get_list_movie_from_api(my_list):
        print ("GET LIST DATA")
        list_movie = []
        for movie in my_list:
            list_movie.append(get_detail_movie(movie))
    
        return list_movie

    ```
1. Create a file `base.html` (can use `base.html` that 
    you've already made on lab 9 including _header_ and _footer_)
1. Create a page for Login
1. Create a page for Dashboard
    ```html
    {% extends "lab_10/layout/base.html" %}
    
    {% block content %}
    <!-- Content Here -->
    <section id="data-dashboard">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h2> Dashboard </h2>
            </div>
            <div class="panel-body">
                <p> Username : {{ author }} </p>
                <p> NPM : {{kode_identitas}} </p>
                <p> Role : {{ role }} </p>
                <p>
                    <a href="{% url 'lab-10:movie_list' %}" class="btn btn-primary"> Daftar Movie </a> |
                    <a href="{% url 'lab-10:list_watch_later' %}" class="btn btn-warning"> Daftar Watch Later </a>
                </p>
            </div>
            <div class="panel-footer">
                <a href="{% url 'lab-10:auth_logout' %}" class="btn btn-danger" onclick="return confirm('Keluar?')">
                    Logout </a>
            </div>
        </div>
    </section>
    <hr>
    
    {% endblock %}
    ```
1. Create a page `detail.html`
    ```html
    {% extends "lab_10/layout/base.html" %}
    
    {% block content %}
    <div class="row">
        <h1> Catatan : Buatlah halaman detail movie sesuai selera dan kreatifitas kalian </h1>
    
        <br>
        <h2> Raw Data (json) </h2>
        <p> {{ movie }} </p>
        <hr>
        <h2> Contoh cara mengambil judul: </h2>
        <h1 style="color:red"> {{ movie.title}} </h1>
    
        <br>
        <div class="box">
            {% if added %}
            <button class="btn btn-success"> Added to Watch Later </button>
            {% else %}
            <a href="{% url 'lab-10:add_watch_later' id %}" class="btn btn-warning"> Add to Watch Later </a>
            {% endif%}
        </div>
    
    
    </div>
    {% endblock %}
    ```
1. Create a page `list.html`
    ```html
    {% extends "lab_10/layout/base.html" %}

    {% block content %}
    <!-- User Login Here -->
    {% if user_login %}
    <a href="" class="btn btn-primary btn-lg"> Logout </a>
    
    {% else %}
    <p>
        <a href="{% url 'lab-10:list_watch_later' %}" class="btn btn-primary btn-lg "> Daftar <em>Watch Later </em> </a>
    </p>
    
    {% endif %}
    <br>
    <!-- List Movie -->
    <div class="panel panel-info">
        <div class="panel-heading">
            <h2> List Movie </h2>
        </div>
        <div class="panel-body">
            <div style="margin:20px; padding:20px; background-color:lightsteelblue; border-radius:3px;">
                <form method="GET" action="{% url 'lab-10:movie_list' %}" class="form-inline">
                    <label> Nama </label> <input type="text" class="form-control" name="judul" placeholder="Judul"
                                                 value="{{judul}}">
                    <label> Tahun </label> <input type="text" class="form-control" name="tahun" placeholder="Tahun "
                                                  value="{{tahun}}">
                    <input type="submit" class="btn btn-primary pull-right">
                </form>
            </div>
            <hr>
            <div class="table table-responsive">
                <table class="table table-hover" id="myTable" style="width: none;">
                    <thead>
                    <th> Judul</th>
                    <th> Tahun</th>
                    <th> Poster</th>
                    <th> Detail</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
    <!-- Jquery script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $('#myTable').DataTable( {
                "ajax": {
                    "dataType" : 'json',
                    "contentType": "application/json; charset=utf-8",
                    "url": {% autoescape off %} {{ jsonUrlDT }} {% endautoescape%} ,
                    "dataSrc":"dataku",
                },
                "columns" : [
                    {"data" : "Title"},
                    {"data" : "Year"},
                    {
                        "data" : "Poster",
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<img src='"+ oData.Poster +"' style='height:50%' class='img-responsive img-thumbnail'/>");
                        }
                    },
                    {
                        "data" : null,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            $(nTd).html("<a href='/lab-10/movie/detail/"+ oData.imdbID +"' class='btn btn-primary'> Detail </a>");
                        }
                    }
                ],
            } );
        });
    </script>
    
    
    {% endblock %}

    ```
1. Create a page `watch_later.html`
    ```html
    {% extends "lab_10/layout/base.html" %}

    {% block content %}
    <div class="row">
        {% for movie in watch_later_movies %}
        <p> {{ movie }} </p>
        {% endfor %}
    </div>
    {% endblock %}
    ``` 

## Checklist
### Mandatory 

1. Authentication :
    1. [ ] I've Implement the Login and Logout function
    1. [ ] I've explained how Django apps knows that every _user_ that access our Apps,
    will be served differently (Data belongs to users will not be accidently exchanged). Show it in code lines
    
1. Dashboard : 
    1. [ ] I've Registered and Used OMDb API to show list of movies
    1. [ ] I've Used DataTables to show list of movies and filtering
    1. [ ] I've Created a movie search function based on Titles and Years
    1. [ ] I've explained how our Django Apps could do _pagination_ and do a search
    
1. Movie Collection
    1. [ ] I've created a "Watch Later" movie lists (stored in session)
    1. [ ] I've created a "Watched" movie lists (stored in session)
    1. [ ] I've explained how our Django Apps could store movie that we wants to watch, without Login first
    1. [ ] I've explained how our Django Apps could store movie that we wants to watch inside the _database_, and differentiate
    the owner of each movie lists. Show it in the code lines

1. I made sure that i have good _Code Coverage_
    1. [ ] If you have not done the configuration to show Code Coverage in Gitlab, you must configure it in Show Code Coverage in Gitlab on README.md 
    1. [ ] Make sure your Code Coverage is 100%.

#### Challenge
1. [ ] Create a page for List My Watch Later Feature
1. [ ] Create a handler if a movie were added manually
    
