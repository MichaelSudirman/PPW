from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Michael Sudirman' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 5, 11) #TODO Implement this, format (Year, Month, Date)
# Create your views here.
mhs_npm = '1706020300'
mhs_hobby = 'playing games and watching youtube'
mhs_desc = 'I am currently a computer science student from FASILKOM Universitas Indonesia'
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'desc' : mhs_desc, 'NPM' : mhs_npm, 'hobby' : mhs_hobby}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0